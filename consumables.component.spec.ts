decribe("on consumable type change human food", () => {
    beforeEach(() => {
        spectator.component.initializeForm();
        spectator.component.onConsumablesTypeChange();
        spectator.component.onConsumablesFormGroup.get('type').setValue(ConsumablesTypeEnum.humanFood);
    })

    it('human food has required FA validator', () => {
        spectator.component.onHumanFDAChange(spectator.component.humanFoodFormGroupName);
        spectator.component.consumablesFormGroup.get(spectator.component.humanFoodFormGroup).get('isFDARegistered').setValue(true);
        const isFdaControl = spectator.component.humanFoodFormGroup.get('humanFoodFormGroup');
        expect(isFdaControl.errors).toHaveProperty('required');
    })
})