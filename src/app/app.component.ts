import { Component, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
// @Output() changed = new EventEmitter<boolean>();

export class AppComponent {
    websiteList: any = ['ItSolutionStuff.com', 'HDTuto.com', 'Nicesnippets.com']
    consumablesList: any = ["Meat/Poultry", "Human Food", "Animal Food"]
    anotherList: any = ["item1", "item2", "item3"]
    meatList: any = ["Meat 1", "Meat 2", "Meat 3"]
    animalList: any = ["Animal 1", "Animal item2", "Animal item3"]
    humanList: any = ["Human 1", "Human 2", "Human 3"]
    form = new FormGroup({
      website: new FormControl('', Validators.required),
      consumable: new FormControl('', Validators.required),
      item: new FormControl('', Validators.required),
    });
    selectedType: string | null;
    
    get f(){
      return this.form.controls;
    }
    
    submit(){
      console.log(this.form.value);
    }
    changeWebsite(e) {
      console.log(e.target.value);
    }
    changeConsumable(e) {
      if(e.target.value === "Meat/Poultry") {
        this.selectedType = this.consumablesList[0];
        console.log("selectedType:", this.selectedType);
      } else if (e.target.value === "Human Food") {
        console.log(`You have selected Consumable ${this.consumablesList[1]}`)
        this.selectedType = this.consumablesList[1];
        console.log("selectedType:", this.selectedType);


      } else if (e.target.value === "Animal Food") {
        this.selectedType = this.consumablesList[2];
        console.log("selectedType:", this.selectedType);


      } else {
        this.selectedType = null;
        console.log("selectedType:", this.selectedType);

      }
    }
    changeAnotherList(e) {
      if(e.target.value === "item1") {
        this.selectedType = this.anotherList[0];
        console.log("selectedType:", this.selectedType);
      } else if (e.target.value === "item2") {
        this.selectedType = this.anotherList[1];
        console.log("selectedType:", this.selectedType);


      } else if (e.target.value === "item3") {
        this.selectedType = this.anotherList[2];
        console.log("selectedType:", this.selectedType);


      } else {
        // this.selectedType = null;
        this.selectedType = null;
        console.log("selectedType:", this.selectedType);

      }
    }

    changeMeatList(e) {
      if(e.target.value === "Meat 1") {
        this.selectedType = this.meatList[0];
        console.log("selectedType:", this.selectedType);
      } else if (e.target.value === "Meat 2") {
        this.selectedType = this.meatList[1];
        console.log("selectedType:", this.selectedType);


      } else if (e.target.value === "item3") {
        this.selectedType = this.meatList[2];
        console.log("selectedType:", this.selectedType);


      } else {
        // this.selectedType = null;
        this.selectedType = null;
        console.log("selectedType:", this.selectedType);

      }
    }

    changeHumanList(e) {
      if(e.target.value === "Human 1") {
        this.selectedType = this.humanList[0];
        console.log("selectedType:", this.selectedType);
      } else if (e.target.value === "Human 2") {
        this.selectedType = this.humanList[1];
        console.log("selectedType:", this.selectedType);


      } else if (e.target.value === "Human 3") {
        this.selectedType = this.humanList[2];
        console.log("selectedType:", this.selectedType);


      } else {
        // this.selectedType = null;
        this.selectedType = null;
        console.log("selectedType:", this.selectedType);

      }
    }

    changeAnimalList(e) {
      if(e.target.value === "Animal 1") {
        this.selectedType = this.animalList[0];
        console.log("selectedType:", this.selectedType);
      } else if (e.target.value === "Animal 2") {
        this.selectedType = this.animalList[1];
        console.log("selectedType:", this.selectedType);


      } else if (e.target.value === "Animal 3") {
        this.selectedType = this.anotherList[2];
        console.log("selectedType:", this.selectedType);


      } else {
        // this.selectedType = null;
        this.selectedType = null;
        console.log("selectedType:", this.selectedType);

      }
    }
    changeFDA(e) {
      console.log(e.target.value);
    }
}

